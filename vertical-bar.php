<?php

$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';

$mt = my_wp_is_mobile() ? get_field('mt_mobile') : get_field('mt');
$mb = my_wp_is_mobile() ? get_field('mb_mobile') : get_field('mb');
$h = my_wp_is_mobile() ? get_field('height_mobile') : get_field('height');

?>

<div id="<?= $id ?>" class="vertical-bar <?= $css ?>" style="height:<?= $h ?>vh;margin-top:<?= $mt ?>vh;margin-bottom:<?= $mb ?>vh;">
    <div class="animed"></div>
    <div class="fixed"></div>
</div>
