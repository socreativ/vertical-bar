"use strict";
docReady(() => {

  const verticalBars = document.querySelectorAll('.vertical-bar');
  verticalBars.forEach(vb => {
    const animPart = vb.children[0];
    gsap.to(animPart, {
      ease: Power2.easeOut, scaleY: 1, scrollTrigger: {
        trigger: vb,
        start: 'top 60%',
        end: 'bottom 40%',
        scrub: true,
      }
    });
  });

});
